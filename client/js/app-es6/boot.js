import {currentInstance} from  './controllers/NegociacaoController';
import {} from  './polyfill/fetch';


let negociacaoController = new currentInstance();

document.querySelector('.form').onsubmit = negociacaoController.adiciona().bind(negociacaoController);

document.querySelector('#importa').onclick = negociacaoController.importa().bind(negociacaoController);
document.querySelector('#apaga').onsubmit = negociacaoController.apaga().bind(negociacaoController);